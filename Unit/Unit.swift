//
//  Unit.swift
//  Unit
//
//  Created by Private on 12/12/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Unit {
    var damage: Int
    var hitPoints: Int
    var hitPointsLimit: Int
    var name: String
    
    init(name: String, hp: Int, dmg: Int) {
        self.name = name
        self.damage = dmg
        self.hitPoints = hp
        self.hitPointsLimit = hp
    }
    
    func ensureIsAlive() -> Bool {
    if ( hitPoints <= 0 ) {
        print ("Unit Is Dead")
        return false
        }
        return true
    }
    
    func takeDamage(dmg: Int) {
        if (ensureIsAlive() == true) {
            hitPoints = hitPoints - dmg
        }
    }
    
    func addHitPoints(hp: Int){
        if (ensureIsAlive() == true)  {
            hitPoints += hp
        
            if ( hitPoints > hitPointsLimit ) {
                hitPoints = hitPointsLimit
            }
        }
    }
    
    func attack(enemy: Unit) {
        if (ensureIsAlive() != false && enemy.ensureIsAlive() != false) {
            enemy.takeDamage(dmg: damage)
            enemy.counterAttack(enemy: self)
        }
    }
    
    func counterAttack(enemy: Unit) {
        if (ensureIsAlive() == true) {
            enemy.takeDamage(dmg: damage/2);
        }
    }
}















