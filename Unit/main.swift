//
//  main.swift
//  Unit
//
//  Created by Private on 12/12/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation
    
let barbarian = Unit(name:"Barbarian",hp:200,dmg: 20)
let knight = Unit(name:"Knight",hp:100,dmg:35)

print("Unit name: \(barbarian.name)")
print("Hit Points: \(barbarian.hitPoints)/\(barbarian.hitPointsLimit)")
print("Damage: \(barbarian.damage)")

print("\nUnit name: \(knight.name)")
print("Hit Points: \(knight.hitPoints)/\(knight.hitPointsLimit)")
print("Damage: \(knight.damage)")
    
print("\nFire in the hole !!!!!!! ")
    
barbarian.attack(enemy: knight);
    
print("\nUnit name: \(barbarian.name)")
print("Hit Points: \(barbarian.hitPoints)/\(barbarian.hitPointsLimit)")
print("Damage: \(barbarian.damage)")

print("\nUnit name: \(knight.name)")
print("Hit Points: \(knight.hitPoints)/\(knight.hitPointsLimit)")
print("Damage: \(knight.damage)")
